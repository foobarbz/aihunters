const app = require('./app');
const babelConfig = require('./babel.config');

const fs = require('fs');
const { resolve } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlBeautifyPlugin = require('@nurminen/html-beautify-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const templates =
  fs
    .readdirSync(app.getResourcePath('templates'))
    .filter(
      fileName =>
        !fileName.startsWith('_')
        && fileName.endsWith(app.src.templates.ext)
    );

const htmlPlugins = templates.map(fileName => {
    fileName = fileName.replace(app.src.templates.ext, '');

    return new HtmlWebpackPlugin({
      inject: true,
      minify: false,
      filename: fileName + '.html',
      template: './src/' + fileName + app.src.templates.ext,
      templateParameters: {
        year: new Date().getFullYear(),
        menu: require('../src/data/menu.json'),
        ...require(`../src/data/${fileName}.json`),
      }
    });
  });

module.exports = {
  stats: 'errors-only',

  entry: {
    main: [
      'normalize.css',

      `./src/index.njk`,

      app.getResourcePath('styles', 'index'),
      app.getResourcePath('scripts', 'index'),
    ]
  },

  output: {
    path: app.output.path,
    publicPath: '/',
  },

  module: {
    rules: [
      // scripts
      {
        test: /\.js$/,
        include: app.src.path,
        use: {
          loader: 'babel-loader',
          options: babelConfig,
        },
      },

      // templates
      {
        test: /\.njk$/,
        use: {
          loader: 'simple-nunjucks-loader',
          options: {
            trimBlocks: true,
            lstripBlocks: true,
            //searchPaths: [
            //  'src',
            //  'src/includes',
            //  'src/includes/navbar'
            //],
            //searchPaths: app.src.path,
            //searchPaths: [
            //  resolve(__dirname, '../src')
            //],
            searchPaths: [ 'src' ],
            // assetsPaths: app.src.images,
          },
        },
      },
    ],
  },

  resolve: {
    modules: [ 'node_modules' ],
    extensions: [ '.js', '.njk', '.json', '.scss' ],
  },

  plugins: [
    new CleanWebpackPlugin(),

    new CopyWebpackPlugin(
      app.copyPluginConfig
    ),

    ...htmlPlugins,

    new HtmlBeautifyPlugin({
      config: {
        html: {
          end_with_newline: true,
          indent_size: 1,
          indent_with_tabs: true,
          indent_inner_html: false,
          preserve_newlines: true,
          max_preserve_newlines: 1,
        },
      },
    }),
  ],

  optimization: {
    minimize: false,
    //runtimeChunk: 'single',
    //splitChunks: {
    //  cacheGroups: {
    //    vendor: {
    //      chunks: 'all',
    //      name: 'vendors',
    //      test: /[\\/]node_modules[\\/]/,
    //    },
    //  },
    //},
  },
};
