const app = require('./app');
const baseConfig = require('./webpack.base');

const { merge } = require('webpack-merge');
const { resolve } = require('path');

const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const svgToMiniDataURI = require('mini-svg-data-uri');

module.exports = merge(baseConfig, {
  mode: 'production',
  target: 'browserslist',
  devtool: false, // hidden-source-map

  output: {
    filename: app.output.js.path + app.output.js.name,
  },

  module: {
    rules: [
      // styles
      {
        test: /\.(css|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { importLoaders: 2 },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                config: resolve(__dirname, 'postcss.config.js'),
              }
            },
          },
          { loader: 'sass-loader' },
        ],
      },

      // assets
      {
        test: /\.(svg)$/i,
        type: 'asset/inline',
        generator: {
          dataUrl: (content) => {
            if (typeof content !== 'string') {
              content = content.toString();
            }

            return svgToMiniDataURI(content);
          },
        },
      },
      {
        test: /\.(png|jpg|gif|woff(2)?)$/i,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            publicPath: (url) => {
              if (/^(src)/.test(url)) {
                return url.replace('src', '..');
              }
            },
            outputPath: (url) => {
              if (/^(src)/.test(url)) {
                return url.replace('src', 'assets');
              }
            },
          },
        },
      },
    ],
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: app.output.css.path + app.output.css.name,
    }),
  ],

  performance: {
    hints: false,
    maxAssetSize: 512000,
    maxEntrypointSize: 512000,
  },

  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
      }),
    ],
  },
})
