const { resolve } = require('path');

module.exports = {
  src: {
    path: resolve(__dirname, '../src/'),

    data: {
      ext: '.json',
      path: '../src/data/',
    },
    images: resolve(__dirname, '../src/img/'),
    styles: {
      ext: '.scss',
      path: '../src/styles/',
    },
    scripts: {
      ext: '.js',
      path: '../src/scripts/',
    },
    templates: {
      ext: '.njk',
      path: '../src/',
    },
  },

  output: {
    path: resolve(__dirname, '../dist/'),

    js: {
      path: 'assets/js/',
      name: 'scripts.min.js',
    },
    css: {
      path: 'assets/css/',
      name: 'styles.min.css',
    },
  },

  copyPluginConfig: {
    patterns: [
      {
        from: './public/favicon/',
        to: 'assets/favicon/',
      },
      {
        from: './src/uploads/',
        to: 'uploads/',
      },
      {
        from: './public/(*)?.*',
        to: '[name].[ext]',
      },
    ],
  },

  getResourcePath: function(resourceType, fileName)  {
    let resource = this.src[resourceType];
    let resourcePath = resource.path;

    if (fileName) {
      let fileExt = resource.ext;

      resourcePath += (fileName + fileExt);
    }

    return resolve(__dirname, resourcePath);
  },
};
