module.exports = {
  plugins: [
    ['rucksack-css', {
      alias: false,
      hexRGBA: true,
      clearFix: false,
      quantityQueries: false,
      shorthandPosition: false,
    }],
    ['autoprefixer', {
      grid: 'autoplace',
    }],
    //['postcss-normalize', {
    //  forceImport: 'sanitize.css',
    //}],
    ['postcss-preset-env', {
      stage: 1,
    }],
    'cssnano',
  ],
};
