const app = require('./app');
const baseConfig = require('./webpack.base');

const { merge } = require('webpack-merge');
const { resolve } = require('path');

module.exports = merge(baseConfig, {
  mode: 'development',
  target: 'web',
  devtool: 'inline-source-map',

  output: {
    filename: '[name].[fullhash].js',
  },

  module: {
    rules: [
      // styles
      {
        test: /\.(css|scss)$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: { importLoaders: 1 },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                config: resolve(__dirname, 'postcss.config.js'),
              }
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                precision: 5,
              },
            },
          },
        ],
      },

      // assets
      {
        test: /\.(svg|png|jpg|gif|woff|woff2)$/i,
        type: 'asset/resource'
      },
    ],
  },

  devServer: {
    open: true,
    port: 8080,
    compress: false,
    stats: 'errors-only',
    historyApiFallback: true,
    contentBase: app.src.path,
  },
})
