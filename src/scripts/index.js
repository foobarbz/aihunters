import IMask from 'imask';

import {
  Swiper,
  Keyboard,
  Mousewheel,
  Navigation,
  Pagination,
  Scrollbar,
  Controller,
  HashNavigation,
} from 'swiper';

Swiper.use([
  Swiper,
  Keyboard,
  Mousewheel,
  Navigation,
  Pagination,
  Scrollbar,
  Controller,
  HashNavigation,
]);

/*
var phoneFields = document.querySelectorAll('[type="tel"]');

fn.loop(phoneFields, function (element) {
  IMask(element, {
    mask: [
      {
        lazy: false,
        startsWith: '7',
        placeholderChar: '_',
        mask: '+7 (000) 000-00-00',
      },{
        lazy: false,
        startsWith: '375',
        placeholderChar: '_',
        mask: '+375 (00) 000-00-00',
      },{
        startsWith: '',
        mask: '0000000000000',
      }
    ],
    dispatch: (appended, dynamicMasked) => {
      let number = (dynamicMasked.value + appended).replace(/\D/g,'');

      return dynamicMasked.compiledMasks.find((m) => {
        return number.indexOf(m.startsWith) === 0;
      });
    },
  });
});
*/


















var fn = {
  index: (element) => {
    let parent = element.parentNode;
    let index = Array.prototype.indexOf.call(parent.children, element);

    return index;
  },


  loop: (elements, callback) => {
    var index;

    for (index = 0; index < elements.length; index++) {
      callback(elements[index], index);
    }
  },
  addEventListeners: (elements, eventName, callback) => {
    fn.loop(elements, (element) => {
      element.addEventListener(eventName, (e) => {
        callback(element, e);
      });
    });
  },
  addEventsListener: (element, eventList, callback) => {
    fn.loop(eventList, (eventName) => {
      element.addEventListener(eventName, (e) => {
        callback(element, e);
      });
    });
  },
};

// landing
const changeLandingScene = (index) => {
  let activeLandingScene = document.querySelector('.swiper-slide-active .landing__scene_active');

  if (activeLandingScene) {
    activeLandingScene.classList.remove('landing__scene_active');

    document
      .querySelector(`.swiper-slide-active .landing__scene:nth-child(${index})`)
      .classList.add('landing__scene_active');
  }
}
function wheel(event) {
  var delta = 0;

  if (event.wheelDelta) {
      delta = event.wheelDelta / 120;
  }
  if (delta) {
      // Отменим текущее событие - событие поумолчанию (скролинг окна).
      if (event.preventDefault) {
          event.preventDefault();
      }
      event.returnValue = false; // для IE

      // если дельта больше 0, то колесо крутят вверх, иначе вниз
  }
}



const setActiveLandingScreen = (swiper) => {
  swiper
    .slides[swiper.activeIndex]
    .classList.add('landing__screen_active');
};

const landingSwiper = new Swiper('.landing', {
  speed: 600,
  nested: true,
  keyboard: true,
  direction: 'vertical',
  allowTouchMove: false,
  mousewheel: {
    thresholdTime: 200,
    releaseOnEdges: true,
  },
  pagination: {
    clickable: true,
    el: '.landing__pages',
  },
  hashNavigation: {
    watchState: true,
  },
  on: {
    afterInit: setActiveLandingScreen,
    slideChange: setActiveLandingScreen,
    transitionEnd: () => {
      document.querySelector('.page')
        .classList.remove('page_shade');
    },
    transitionStart: () => {
      document.querySelector('.page')
        .classList.add('page_shade');
    },
  },
});

let jsShowScreen = document.querySelectorAll('.js-show-screen');
fn.addEventListeners(jsShowScreen, 'click', (element) => {
    let target = element.getAttribute('data-screen');
    let screen = document.querySelector(`[data-hash="${target}"]`);

    if (screen) {
        let index;

        index = fn.index(screen);
        landingSwiper.slideTo(index);
    }
});



// popup
{
  let elPopups = document.querySelector('.popups');
  let elsOpenHook = document.querySelectorAll('.js-open-popup');
  let elsCloseHook = document.querySelectorAll('.js-close-popup');

  fn.addEventListeners(elsOpenHook, 'click', (element) => {
    let id = element.getAttribute('data-popup');
    let scene = document.querySelector(`[data-popup-id="${id}"]`);

    document.body.style.overflow = 'hidden';

    elPopups.classList.add('popups_show');
    scene.classList.add('popups__scene_show');

    setTimeout(() => {
      scene.style.overflow = 'auto';
    }, 600);
  });

  fn.addEventListeners(elsCloseHook, 'click', () => {
    let scene = document.querySelector('.popups__scene_show');

    document.body.style.overflow = '';

    elPopups.classList.remove('popups_show');
    scene.classList.add('popups__scene_hide');
    scene.style.overflow = '';

    setTimeout(() => {
      scene.classList.remove('popups__scene_show');
      scene.classList.remove('popups__scene_hide');
    }, 200);
  });
}

// activity
const activitySwiper = new Swiper('.activity__show', {
  loop: true,
  keyboard: true,
  grabCursor: true,
  on: {
    activeIndexChange: (swiper) => {
      let index = swiper.realIndex + 1;

      changeLandingScene(index);
    },
  },
  navigation: {
    prevEl: '.activity__arrow_prev',
    nextEl: '.activity__arrow_next',
  },
  pagination: {
    clickable: true,
    el: '.activity__pages',
  },
});

// solutions
{
  let config = {
    keyboard: true,
    mousewheel: true,
    grabCursor: true,
    spaceBetween: 20,
    slidesPerView: 2,
    on: {
      transitionEnd: () => {
        landingSwiper.mousewheel.enable();
      },
      beforeTransitionStart: () => {
        landingSwiper.mousewheel.disable();
      },
    },
  };
  const elements = document.querySelectorAll('.solutions__show');

  const show1 = new Swiper(elements[0], config);
  const show2 = new Swiper(elements[1], {
    ...config,
    scrollbar: {
      dragSize: 14,
      draggable: true,
      el: '.solutions__scrollbar',
    },
  });

  show1.controller.control = show2;
  show2.controller.control = show1;
};





const body = document.body;

document.addEventListener('DOMContentLoaded', () => {
  body.classList.remove('page_loading');
});
